+++
date = "2018-09-23T11:06:29+03:00"
title = "Преобразование координат из WGS84 в МСК-50 с помощью GO"

+++


Недавно возникла задача перевода координат из системы [WGS84](https://ru.wikipedia.org/wiki/WGS_84) в [Московскую систему координат (МГГТ)](https://mapbasic.ru/mggt). Сложность была в том, что в текущий момент для этих целей использовалась библиоткета на Fortran, которая через обертку вызывалась из Python.
В данный момент я начал переход на Go и тянуть данную библиотеку очень не хотелось.

Одни мой [знакомый](https://www.linkedin.com/in/dmitriy-litvinov-20b75894), сказал что для этого можно использовать библиотеку [Proj4](https://proj4.org), так как для нее есть go биндинг [go-proj-4](https://github.com/pebbe/go-proj-4/proj).

Итоговая функция приведена ниже:

```
func WGStoMSK(lon, lat float64) (float64, float64, error) {
	var (
		x, y             float64
		err              error
		mskProj, wgsProj *proj.Proj
	)

	/* Система координат Московская СК (МГГТ) города Москвы и МО в формате proj4
	парематры x_0 и y_0 подобраны вручную. Источник:http://www.mapbasic.ru/mggt
	*/
	mskProj, err = proj.NewProj("+proj=tmerc +lat_0=55.66666666667 +lon_0=37.5 +k=1 +x_0=11.86143 +y_0=12.1761150 +ellps=bessel +towgs84=316.151,78.924,589.65,-1.57273,2.69209,2.34693,8.4507 +units=m +no_defs")
	defer mskProj.Close()
	if err != nil {
		return x, y, fmt.Errorf("Error msk proj create: %v\n", err)
	}
	// Система координат WGS-84 в формате proj4
	wgsProj, err = proj.NewProj("+proj=longlat +datum=WGS84 +no_defs")
	defer wgsProj.Close()
	if err != nil {
		return x, y, fmt.Errorf("Error wgs proj create: %v\n", err)
	}

	x, y, err = proj.Transform2(wgsProj, mskProj, proj.DegToRad(lon), proj.DegToRad(lat))
	if err != nil {
		return x, y, fmt.Errorf("Error transform: %v\n", err)
	}

	return x, y, err
}
```

Кроме того можно добавить систему координат в Postgis:
```
INSERT INTO public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) VALUES (100001, 'EGKO', 100001, 'PROJCS["Transverse_Mercator",GEOGCS["GCS_Bessel 1841",DATUM["D_unknown",SPHEROID["bessel",6377397.155,299.1528128]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",55.66666666667],PARAMETER["central_meridian",37.5],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]', '+proj=tmerc +ellps=bessel +towgs84=367.93,88.45,553.73,-0.8777,1.3231,2.6248,8.96 +units=m +lon_0=37.5 +lat_0=55.66666666667 +k_0=1 +x_0=0 +y_0=0');
```