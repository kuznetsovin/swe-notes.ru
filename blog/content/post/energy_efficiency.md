+++
date = "2013-12-16T16:26:13+03:00"
draft = false
title = "Пример решения задачи множественной регрессии с помощью Python"

+++

#### Введение

Добрый день, уважаемые читатели.

В прошлых статьях, на практических примерах, мной были показаны способы решения задач классификации (<a href='http://habrahabr.ru/post/204500/'>задача кредитного скоринга</a>) и основ анализа текстовой информации (<a href='http://habrahabr.ru/post/205360/'>задача о паспортах</a>). Сегодня же мне бы хотелось коснуться другого класса задач, а именно <a href='http://www.machinelearning.ru/wiki/index.php?title=%D0%A0%D0%B5%D0%B3%D1%80%D0%B5%D1%81%D1%81%D0%B8%D1%8F'>восстановления регрессии</a>.
Задачи данного класса, как правило, используются при <a href='http://www.machinelearning.ru/wiki/index.php?title=%D0%9F%D1%80%D0%BE%D0%B3%D0%BD%D0%BE%D0%B7%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5'>прогнозировании</a>.

Для примера решения задачи прогнозирования, я взял набор данных <a href='http://archive.ics.uci.edu/ml/datasets/Energy+efficiency'>Energy efficiency</a> из крупнейшего репозитория <a href='http://www.machinelearning.ru/wiki/index.php?title=UCI'>UCI</a>. В качестве инструментов по традиции будем использовать Python c аналитическими пакетами <a href='http://pandas.pydata.org/pandas-docs/stable/'>pandas</a> и <a href='http://scikit-learn.org/stable/'>scikit-learn</a>.

#### Описание набора данных и постановка задачи

Дан <a href='http://archive.ics.uci.edu/ml/machine-learning-databases/00242/ENB2012_data.xlsx'>набор данных</a>, котором описаны следующие атрибуты помещения:
<table>
<tr><th>Поле</th><th>Описание</th><th>Тип</th></tr>
<tr><th>X1</th><td>Относительная компактность</td><td>FLOAT</td><tr>
<tr><th>X2</th><td>Площадь</td><td>FLOAT</td><tr>
<tr><th>X3</th><td>Площадь стены</td><td>FLOAT</td><tr>
<tr><th>X4</th><td>Площадь потолка</td><td>FLOAT</td><tr>
<tr><th>X5</th><td>Общая высота</td><td>FLOAT</td><tr>
<tr><th>X6</th><td>Ориентация</td><td>INT</td><tr>
<tr><th>X7</th><td>Площадь остекления</td><td>FLOAT</td><tr>
<tr><th>X8</th><td>Распределенная площадь остекления</td><td>INT</td><tr>
<tr><th>y1</th><td>Нагрузка при обогреве</td><td>FLOAT</td><tr>
<tr><th>y2</th><td>Нагрузка при охлаждении</td><td>FLOAT</td><tr>
</table>

В нем $X1 \dotsm X8$ - характеристики помещения на основании которых будет проводиться анализ, а $y1, y2$ - значения нагрузки, которые надо спрогнозировать.

#### Предварительный анализ данных

Для начала загрузим наши данные и помотрим на них:


```
from pandas import read_csv, DataFrame
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
from sklearn.cross_validation import train_test_split
```


```
dataset = read_csv('EnergyEfficiency/ENB2012_data.csv',';')
dataset.head()
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>X1</th>
      <th>X2</th>
      <th>X3</th>
      <th>X4</th>
      <th>X5</th>
      <th>X6</th>
      <th>X7</th>
      <th>X8</th>
      <th>Y1</th>
      <th>Y2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td> 0.98</td>
      <td> 514.5</td>
      <td> 294.0</td>
      <td> 110.25</td>
      <td> 7</td>
      <td> 2</td>
      <td> 0</td>
      <td> 0</td>
      <td> 15.55</td>
      <td> 21.33</td>
    </tr>
    <tr>
      <th>1</th>
      <td> 0.98</td>
      <td> 514.5</td>
      <td> 294.0</td>
      <td> 110.25</td>
      <td> 7</td>
      <td> 3</td>
      <td> 0</td>
      <td> 0</td>
      <td> 15.55</td>
      <td> 21.33</td>
    </tr>
    <tr>
      <th>2</th>
      <td> 0.98</td>
      <td> 514.5</td>
      <td> 294.0</td>
      <td> 110.25</td>
      <td> 7</td>
      <td> 4</td>
      <td> 0</td>
      <td> 0</td>
      <td> 15.55</td>
      <td> 21.33</td>
    </tr>
    <tr>
      <th>3</th>
      <td> 0.98</td>
      <td> 514.5</td>
      <td> 294.0</td>
      <td> 110.25</td>
      <td> 7</td>
      <td> 5</td>
      <td> 0</td>
      <td> 0</td>
      <td> 15.55</td>
      <td> 21.33</td>
    </tr>
    <tr>
      <th>4</th>
      <td> 0.90</td>
      <td> 563.5</td>
      <td> 318.5</td>
      <td> 122.50</td>
      <td> 7</td>
      <td> 2</td>
      <td> 0</td>
      <td> 0</td>
      <td> 20.84</td>
      <td> 28.28</td>
    </tr>
  </tbody>
</table>
</div>



Теперь давайте посмортим не связаны ли между собой какие-либо атрибуты. Сделать это можно расчитав коэффициенты корреляции для всех столбцов. Как это сделать было описано в предыдущей <a href='http://habrahabr.ru/post/204500/'>статье</a>:


```
dataset.corr()
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>X1</th>
      <th>X2</th>
      <th>X3</th>
      <th>X4</th>
      <th>X5</th>
      <th>X6</th>
      <th>X7</th>
      <th>X8</th>
      <th>Y1</th>
      <th>Y2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>X1</th>
      <td> 1.000000e+00</td>
      <td>-9.919015e-01</td>
      <td>-2.037817e-01</td>
      <td>-8.688234e-01</td>
      <td> 8.277473e-01</td>
      <td> 0.000000</td>
      <td> 1.283986e-17</td>
      <td> 1.764620e-17</td>
      <td> 0.622272</td>
      <td> 0.634339</td>
    </tr>
    <tr>
      <th>X2</th>
      <td>-9.919015e-01</td>
      <td> 1.000000e+00</td>
      <td> 1.955016e-01</td>
      <td> 8.807195e-01</td>
      <td>-8.581477e-01</td>
      <td> 0.000000</td>
      <td> 1.318356e-16</td>
      <td>-3.558613e-16</td>
      <td>-0.658120</td>
      <td>-0.672999</td>
    </tr>
    <tr>
      <th>X3</th>
      <td>-2.037817e-01</td>
      <td> 1.955016e-01</td>
      <td> 1.000000e+00</td>
      <td>-2.923165e-01</td>
      <td> 2.809757e-01</td>
      <td> 0.000000</td>
      <td>-7.969726e-19</td>
      <td> 0.000000e+00</td>
      <td> 0.455671</td>
      <td> 0.427117</td>
    </tr>
    <tr>
      <th>X4</th>
      <td>-8.688234e-01</td>
      <td> 8.807195e-01</td>
      <td>-2.923165e-01</td>
      <td> 1.000000e+00</td>
      <td>-9.725122e-01</td>
      <td> 0.000000</td>
      <td>-1.381805e-16</td>
      <td>-1.079129e-16</td>
      <td>-0.861828</td>
      <td>-0.862547</td>
    </tr>
    <tr>
      <th>X5</th>
      <td> 8.277473e-01</td>
      <td>-8.581477e-01</td>
      <td> 2.809757e-01</td>
      <td>-9.725122e-01</td>
      <td> 1.000000e+00</td>
      <td> 0.000000</td>
      <td> 1.861418e-18</td>
      <td> 0.000000e+00</td>
      <td> 0.889431</td>
      <td> 0.895785</td>
    </tr>
    <tr>
      <th>X6</th>
      <td> 0.000000e+00</td>
      <td> 0.000000e+00</td>
      <td> 0.000000e+00</td>
      <td> 0.000000e+00</td>
      <td> 0.000000e+00</td>
      <td> 1.000000</td>
      <td> 0.000000e+00</td>
      <td> 0.000000e+00</td>
      <td>-0.002587</td>
      <td> 0.014290</td>
    </tr>
    <tr>
      <th>X7</th>
      <td> 1.283986e-17</td>
      <td> 1.318356e-16</td>
      <td>-7.969726e-19</td>
      <td>-1.381805e-16</td>
      <td> 1.861418e-18</td>
      <td> 0.000000</td>
      <td> 1.000000e+00</td>
      <td> 2.129642e-01</td>
      <td> 0.269841</td>
      <td> 0.207505</td>
    </tr>
    <tr>
      <th>X8</th>
      <td> 1.764620e-17</td>
      <td>-3.558613e-16</td>
      <td> 0.000000e+00</td>
      <td>-1.079129e-16</td>
      <td> 0.000000e+00</td>
      <td> 0.000000</td>
      <td> 2.129642e-01</td>
      <td> 1.000000e+00</td>
      <td> 0.087368</td>
      <td> 0.050525</td>
    </tr>
    <tr>
      <th>Y1</th>
      <td> 6.222722e-01</td>
      <td>-6.581202e-01</td>
      <td> 4.556712e-01</td>
      <td>-8.618283e-01</td>
      <td> 8.894307e-01</td>
      <td>-0.002587</td>
      <td> 2.698410e-01</td>
      <td> 8.736759e-02</td>
      <td> 1.000000</td>
      <td> 0.975862</td>
    </tr>
    <tr>
      <th>Y2</th>
      <td> 6.343391e-01</td>
      <td>-6.729989e-01</td>
      <td> 4.271170e-01</td>
      <td>-8.625466e-01</td>
      <td> 8.957852e-01</td>
      <td> 0.014290</td>
      <td> 2.075050e-01</td>
      <td> 5.052512e-02</td>
      <td> 0.975862</td>
      <td> 1.000000</td>
    </tr>
  </tbody>
</table>
</div>



Как можно заметить из нашей матрицы, коррелируют между собой следующие столбы (Значение коэффициента корреляции больше 95%):
<ul>
<li>y1 --> y2</li>
<li>x1 --> x2</li>
<li>x4 --> x5</li>
</ul>

Теперь давайте выберем, какике столбцы их наших пар мы можем убрать из нашей выборки. Для этого, в каждой паре, выберем столбцы, которые в большей степени оказывают влияние на прогнозные значения <i>Y1</i> и <i>Y2</i> и оставим их, а остальные удалим.

Как можно заметить и матрицы с коэффициентами корреляции на <i><b>y1</b></i>,<i><b>y2</b></i> больше значения оказывают <b><i>X2</i></b> и <b><i>X5</i></b>, нежели X1 и X4, таким образом мы можем  последние столбцы мы можем удалить.


```
dataset = dataset.drop(['X1','X4'], axis=1)
dataset.head()
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>X2</th>
      <th>X3</th>
      <th>X5</th>
      <th>X6</th>
      <th>X7</th>
      <th>X8</th>
      <th>Y1</th>
      <th>Y2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td> 514.5</td>
      <td> 294.0</td>
      <td> 7</td>
      <td> 2</td>
      <td> 0</td>
      <td> 0</td>
      <td> 15.55</td>
      <td> 21.33</td>
    </tr>
    <tr>
      <th>1</th>
      <td> 514.5</td>
      <td> 294.0</td>
      <td> 7</td>
      <td> 3</td>
      <td> 0</td>
      <td> 0</td>
      <td> 15.55</td>
      <td> 21.33</td>
    </tr>
    <tr>
      <th>2</th>
      <td> 514.5</td>
      <td> 294.0</td>
      <td> 7</td>
      <td> 4</td>
      <td> 0</td>
      <td> 0</td>
      <td> 15.55</td>
      <td> 21.33</td>
    </tr>
    <tr>
      <th>3</th>
      <td> 514.5</td>
      <td> 294.0</td>
      <td> 7</td>
      <td> 5</td>
      <td> 0</td>
      <td> 0</td>
      <td> 15.55</td>
      <td> 21.33</td>
    </tr>
    <tr>
      <th>4</th>
      <td> 563.5</td>
      <td> 318.5</td>
      <td> 7</td>
      <td> 2</td>
      <td> 0</td>
      <td> 0</td>
      <td> 20.84</td>
      <td> 28.28</td>
    </tr>
  </tbody>
</table>
</div>



Помимо этого, можно заметить, что поля <i><b>Y1</b></i> и <i><b>Y2</b></i> очень тесно коррелируют между собой. Но, т. к. нам надо спрогнозировать оба значения мы их оставляем "как есть".

#### Выбор модели

Отделим от нашей выборки прогнозные значения:


```
trg = dataset[['Y1','Y2']]
trn = dataset.drop(['Y1','Y2'], axis=1)
```

После обработки данных можно перейти к построению модели. Для построения модели будем использовать следующие методы:
<ul>
<li><a href='http://ru.wikipedia.org/wiki/%CC%E5%F2%EE%E4_%ED%E0%E8%EC%E5%ED%FC%F8%E8%F5_%EA%E2%E0%E4%F0%E0%F2%EE%E2'>Метод наименьших квадратов</a></li>
<li><a href='http://ru.wikipedia.org/wiki/Random_forest'>Случайный лес</a></li>
<li><a href='http://ru.wikipedia.org/wiki/%D0%9B%D0%BE%D0%B3%D0%B8%D1%81%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D1%80%D0%B5%D0%B3%D1%80%D0%B5%D1%81%D1%81%D0%B8%D1%8F'>Логистическую регрессию</a></li>
<li><a href='http://ru.wikipedia.org/wiki/%CC%E5%F2%EE%E4_%EE%EF%EE%F0%ED%FB%F5_%E2%E5%EA%F2%EE%F0%EE%E2'>Метод опорных векторов</a></li>
<li><a href='http://www.machinelearning.ru/wiki/index.php?title=KNN'>Метод ближайших соседей</a></li>
</ul>

Теорию о данным методам можно почитать в <a href='http://www.machinelearning.ru/wiki/index.php?title=%D0%9C%D0%B0%D1%88%D0%B8%D0%BD%D0%BD%D0%BE%D0%B5_%D0%BE%D0%B1%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5_(%D0%BA%D1%83%D1%80%D1%81_%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B9%2C_%D0%9A.%D0%92.%D0%92%D0%BE%D1%80%D0%BE%D0%BD%D1%86%D0%BE%D0%B2)'>курсе лекций К.В.Воронцова по машинному обучению</a>.

Оценку будем производить с помощью <a href='http://ru.wikipedia.org/wiki/%CA%EE%FD%F4%F4%E8%F6%E8%E5%ED%F2_%E4%E5%F2%E5%F0%EC%E8%ED%E0%F6%E8%E8'>коэффициента детерминации</a> (<i>R-квадрат</i>). Данный коэффициент определяется следующим образом:

$$R^2 = 1 - \frac{V(y|x)}{V(y)} = 1 - \frac{\sigma^2}{\sigma_y^2}$$

,где $V(y|x) = {\sigma^2}$ - условная дисперсия зависимой величины <i>у</i> по фактору <i>х</i>

Коэффициент принимает значение на промежутке $[0,1]$ и чем он ближе к 1 тем сильнее зависимость.

Ну что же теперь можно перейти непосредственно к построению модели и выбору модели. Давайте поместим все наши модели в один список для удобства дальнейшего анализа:


```
models = [LinearRegression(), # метод наименьших квадратов
          RandomForestRegressor(n_estimators=100, max_features ='sqrt'), # случайный лес
          KNeighborsRegressor(n_neighbors=6), # метод ближайших соседей
          SVR(kernel='linear'), # метод опорных векторов с линейным ядром
          LogisticRegression() # логистическая регрессия
          ]
```

Итак модели готовы, теперь мы разобъем наши исходные данные на 2 подвыборки: <i>тестовую</i> и <i>обучающую</i>. Кто читал мои предыдущие статьи знает, что сделать это можно с помощью функции <a hrfe='http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.train_test_split.html'>train_test_split()</a> из пакета scikit-learn:


```
Xtrn, Xtest, Ytrn, Ytest = train_test_split(trn, trg, test_size=0.4)
```

Теперь, т. к. нам надо спрогнозировать 2 параметра $y1,y2$, надо построить регрессию для каждого из них. Кроме этого, для дальнейшего анализа, можно записать полученные результаты во временный <i>DataFrame</i>. Сделать это можно так:


```
#создаем временные структуры
TestModels = DataFrame()
tmp = {}
#для каждой модели из списка
for model in models:
    #получаем имя модели
    m = str(model)
    tmp['Model'] = m[:m.index('(')]    
    #для каждого столбцам результирующего набора
    for i in  xrange(Ytrn.shape[1]):
        #обучаем модель
        model.fit(Xtrn, Ytrn[:,i]) 
        #вычисляем коэффициент детерминации
        tmp['R2_Y%s'%str(i+1)] = r2_score(Ytest[:,0], model.predict(Xtest))
    #записываем данные и итоговый DataFrame
    TestModels = TestModels.append([tmp])
#делаем индекс по названию модели
TestModels.set_index('Model', inplace=True)
```

Как можно заметить из кода выше, для роасчета коэффициента $R^2$ используется функция <a hrfe='http://scikit-learn.org/stable/modules/generated/sklearn.metrics.r2_score.html'>r2_score()</a>.

Итак, данные для анализа получены. Давайте теперь построим графики и посмотрим какая модель показала лучший результат:


```
fig, axes = plt.subplots(ncols=2, figsize=(10,4))
TestModels.R2_Y1.plot(ax=axes[0], kind='bar', title='R2_Y1')
TestModels.R2_Y2.plot(ax=axes[1], kind='bar', color='green', title='R2_Y2')
```




    <matplotlib.axes.AxesSubplot at 0x6107af0>




![png](/resource/energy_efficiency_files/EnergyEfficiency_36_1.png)


#### Анализ результатов и выводы

Из графиков, приведенных выше, можно сделать вывод, что лучше других с задачей справился метод <i>RandomForest</i> (случайный лес). Его коэффициенты детерминации выше остальных по обоим переменным: $R_{y1}^2 \approx 99\%,\ R_{y2}^2 \approx 90\%$

Для дальнейшего анализа давайте заново обучим нашу модель:


```
model = models[1]
model.fit(Xtrn, Ytrn)
```




    RandomForestRegressor(bootstrap=True, compute_importances=None,
               criterion='mse', max_depth=None, max_features='sqrt',
               min_density=None, min_samples_leaf=1, min_samples_split=2,
               n_estimators=100, n_jobs=1, oob_score=False, random_state=None,
               verbose=0)



При внимательном рассмотрении, может возникнуть вопрос, почему в предыдущий раз и делили зависимую выборку <i>Ytrn</i> на переменные(по столбцамм), а теперь мы этого не делаем.

Дело в том, что некоторые методы, такие как <i>RandomForestRegressor</i>, может работать с несколькими прогнозируемыми переменными, а другие (например <i>SVR</i>) могут работать только с одной переменной. Поэтому на при предыдущем обучении мы использовали разбиение по столбцам, чтобы избежать ошибки в процессе построения некоторых моделей.

Выбрать модель это, конечно же, хорошо, но еще неплохо бы обладать информацией, как каждый фактор влиет на прогнозное значение. Для этого у модели есть свойство <i>feature\_importances\_</i>.

С помощью него, можно посмотреть вес каждого фактора в итоговой моделе:


```
model.feature_importances_
```




    array([ 0.40717901,  0.11394948,  0.34984766,  0.00751686,  0.09158358,
            0.02992342])



В нашем случае видно, что больше всего на нагрузку при обогреве и охлаждении влияют общая высота и площадь. Их общий вклад в прогнозной модели около 72%.

Также необходимо отметить, что по вышеуказанной схеме можно посмотреть влияне каждого фактора одельно на обогрев и отдельно на охлаждение, но т. к. эти факторы у нас очень тесно коррелируют между собой  ($r\ =\ 97\%$), мы следали общий вывод по ним обоим который и был написан выше.

#### Заключение

В статье я постарался показать основные этапы при регрессионном анализе данных с помощью Python и аналитческих пакетов <b>pandas</b> и <b>scikit-learn</b>. 

Необходимо отметить, что набор данных специально выбирался таким образом чтобы быть максимально формализованым и первичная обработка входных данных была бы минимальна. На мой взгляд статья будет полезна тем, кто только начинает свой путь в анализе данных, а также тем кто имеет хорошую теоритическую базу, но выбирает инструментарий для работы.

Файл консоли IPython Notebook: <a href='#'>EnergyEfficiency.ipynb</a>
