+++
date = "2013-11-14T13:26:13+03:00"
draft = false
title = "Введение в анализ данных с помощью python на примере задачи про Титаник"

+++

<a href='https://www.kaggle.com/c/titanic-gettingStarted/data'>Задача</a> про спасенных с "Титаника", имеет большую популярность среди людей, только начинающих заниматься анализом данных и <a href='http://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D1%88%D0%B8%D0%BD%D0%BD%D0%BE%D0%B5_%D0%BE%D0%B1%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5'>машинным обучением</a>.

Итак суть задачи состоит в том, чтобы с помощью методов машинного обучения построить модель, которая прогназировала бы спасется человек или нет. К задачи прилагаются 2 файла: <ul><li><i>train.csv</i> - набор данных на основании которого будет строиться модель (<i>обучающая выборка</i></li><li><i>test.csv</i> - набор данных для проверки модели</li></ul>

Для анализ понадобятся модули <a href='http://pandas.pydata.org/'>Pandas</a> и <a href='http://scikit-learn.org/stable/'>sklearn</a>. С помощью <b>Pandas</b> мы проведем начальный анализ данных, а <b>sklearn</b> поможет в вычислении прогнозной модели. Итак, для начала загрузим нужные модули:

Кроме того даются пояснения по некоторым полям:<ul><li><b>PassengerId</b> - идентификатор пассажира</li><li><b>Survival</b> - поле в котором указано спасся человек(1) или нет (0)</li><li><b>Pclass</b> - содержит социально-экономический статус: <ol><li>высокий</li><li>средний</li><li>низкий</li></ol></li><li><b>Name</b> - имя пассажира</li><li><b>Sex</b> - пол пассажира</li><li><b>Age</b> - возраст</li><li><b>SibSp</b> - содержит информацию о количестве родственников 2-го порядка (муж, жена, братья, сетры)</li><li><b>Parch</b> - содержит информацию о количестве родственников на борту 1-го порядка (мать, отец, дети)</li><li><b>Ticket</b> - номер билета</li><li><b>Fare</b> - цена билета</li><li><b>Cabin</b> - каюта</li><li><b>Embarked</b> - порт посадки<ul><li>C - Cherbourg</li><li>Q - Queenstown</li><li>S - Southampton</li></ul></li></ul>


```
from pandas import read_csv, DataFrame, Series
```

Теперь можно загрузить тестовую выборку и посмотреть на нее:


```
data = read_csv('Kaggle_Titanic/Data/train.csv')
data
```




<pre>
&lt;class 'pandas.core.frame.DataFrame'&gt;
Int64Index: 891 entries, 0 to 890
Data columns (total 12 columns):
PassengerId    891  non-null values
Survived       891  non-null values
Pclass         891  non-null values
Name           891  non-null values
Sex            891  non-null values
Age            714  non-null values
SibSp          891  non-null values
Parch          891  non-null values
Ticket         891  non-null values
Fare           891  non-null values
Cabin          204  non-null values
Embarked       889  non-null values
dtypes: float64(2), int64(5), object(5)
</pre>



Можно предположить, что чем выше социальный статус, тем больше вероятность спасения. Давайте проверим это взглянув на количество спасшихся и утонувших в зависимости в разрезе классов. Для этого нужно построить следующую сводную:


```
data.pivot_table('PassengerId', 'Pclass', 'Survived', 'count').plot(kind='bar', stacked=True)
```




    <matplotlib.axes.AxesSubplot at 0x3c57070>




![png](/resource/titanic_test_post_files/Titanic_test_post_9_1.png)


Наше вышеописанное предположение про то, что чем выше у пассажиров их социальное положение, тем выше их вероятность спасения. Теперь давайте взглямен, как количество родственников влияет на факт спасения:


```
fig, axes = plt.subplots(ncols=2)
data.pivot_table('PassengerId', ['SibSp'], 'Survived', 'count').plot(ax=axes[0], title='SibSp')
data.pivot_table('PassengerId', ['Parch'], 'Survived', 'count').plot(ax=axes[1], title='Parch')
```

    <matplotlib.axes.AxesSubplot at 0x3cea970>

![png](/resource/titanic_test_post_files/Titanic_test_post_11_1.png)


Как видно из графиков наше предположение снова потдвердилось, и из людей имеющих больше 1 родственников спаслись не многие.

Теперь давайте взглянем столбец с номерами кают и посмотрим насколько нам пригодятся эти данные:


```
data.PassengerId[data.Cabin.notnull()].count()
```

    204

Как видно поле практически не запонено, поэтому данные по нему можно будет опустить.

Итак продолжем разбираться с полями и на очереди поле Age в котором записан возраст. Посмортирим на сколько оно заполено:


```
data.PassengerId[data.Age.notnull()].count()
```

    714


Данное поле практически все заполнено, но есть пустые значения, которые не определены. Давайте зададим таким поляем значение равное медиане по возрату из всей выборки. Данный шаг нужен для более точного построения модели:


```
data.Age = data.Age.median()
```

У нас осталось разобратся с полями <i>Ticket</i>, <i>Embarked</i>, <i>Fare</i>, <i>Name</i>. Давайте посмотрим на поле Embarked, в котором находится порт посадки и проверим есть ли такие пассажиры у которых порт не указан:


```
data[data.Embarked.isnull()]
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Name</th>
      <th>Sex</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Ticket</th>
      <th>Fare</th>
      <th>Cabin</th>
      <th>Embarked</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>61 </th>
      <td>  62</td>
      <td> 1</td>
      <td> 1</td>
      <td>                       Icard, Miss. Amelie</td>
      <td> female</td>
      <td> 28</td>
      <td> 0</td>
      <td> 0</td>
      <td> 113572</td>
      <td> 80</td>
      <td> B28</td>
      <td> NaN</td>
    </tr>
    <tr>
      <th>829</th>
      <td> 830</td>
      <td> 1</td>
      <td> 1</td>
      <td> Stone, Mrs. George Nelson (Martha Evelyn)</td>
      <td> female</td>
      <td> 28</td>
      <td> 0</td>
      <td> 0</td>
      <td> 113572</td>
      <td> 80</td>
      <td> B28</td>
      <td> NaN</td>
    </tr>
  </tbody>
</table>
</div>



Итак у нас нашлось 2 таких пассажира. Давайте присвоим эти пассажирам порт в котором село больше всего людей:


```
MaxPassEmbarked = data.groupby('Embarked').count()['PassengerId']
data.Embarked[data.Embarked.isnull()] = MaxPassEmbarked[MaxPassEmbarked == MaxPassEmbarked.max()].index[0]
```

Ну что же разобрались еще с одним полем и теперь у нас остались поля с имя пассажира, номером билета и ценой билета.

По сути нам из этих трех полей нам нужна только цена(Fare), т.к. она в какой-то мере определяем ранжирование внутри классов поля Pclass. Т. е. например люди внутри среднего класса могут быть разделены на тех, кто ближе к первому(высшему) классу, а кто к третьему(низший). Проверим это поле на пустые значения и если таковые имеются заменим цену медианой по цене из все выборки:


```
data.PassengerId[data.Fare.isnull()]
```




    Series([], dtype: int64)



Номер же билета и имя пассажира нам никак не помогут, т. к. это просто справочная информация. Единственное для чего они могут пригодиться -  это определение кто из пассажиров потенциально являются родственниками, но так как люди у которых есть родственники практически не спаслись (это было показано выше) можно принебречь этими данными.

Теперь наш набор выглядит так: 


```
data = data.drop(['PassengerId','Name','Ticket','Cabin'],axis=1)
data.head()
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Sex</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Fare</th>
      <th>Embarked</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td> 0</td>
      <td> 3</td>
      <td>   male</td>
      <td> 28</td>
      <td> 1</td>
      <td> 0</td>
      <td>  7.2500</td>
      <td> S</td>
    </tr>
    <tr>
      <th>1</th>
      <td> 1</td>
      <td> 1</td>
      <td> female</td>
      <td> 28</td>
      <td> 1</td>
      <td> 0</td>
      <td> 71.2833</td>
      <td> C</td>
    </tr>
    <tr>
      <th>2</th>
      <td> 1</td>
      <td> 3</td>
      <td> female</td>
      <td> 28</td>
      <td> 0</td>
      <td> 0</td>
      <td>  7.9250</td>
      <td> S</td>
    </tr>
    <tr>
      <th>3</th>
      <td> 1</td>
      <td> 1</td>
      <td> female</td>
      <td> 28</td>
      <td> 1</td>
      <td> 0</td>
      <td> 53.1000</td>
      <td> S</td>
    </tr>
    <tr>
      <th>4</th>
      <td> 0</td>
      <td> 3</td>
      <td>   male</td>
      <td> 28</td>
      <td> 0</td>
      <td> 0</td>
      <td>  8.0500</td>
      <td> S</td>
    </tr>
  </tbody>
</table>
</div>



Итак для построения нашей модели, нужно закодировать все наши текстовые значения. Можно это сделать в ручную, а можно с помощью модуля <a href='http://scikit-learn.org/stable/modules/preprocessing.html#preprocessing'>sklearn.preprocessing</a>. Давайте воспользуемся вторым вариантом.

Закодировать список с фиксированными значениями можно с помощью объекта <a href='http://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.LabelEncoder.html#sklearn.preprocessing.LabelEncoder'>LabelEncoder()</a>. 


```
from sklearn.preprocessing import LabelEncoder
label = LabelEncoder()
dicts = {}

label.fit(data.Sex.drop_duplicates())
dicts['Sex'] = list(label.classes_)
data.Sex = label.transform(data.Sex)

label.fit(data.Embarked.drop_duplicates())
dicts['Embarked'] = list(label.classes_)
data.Embarked = label.transform(data.Embarked)
```

В итоге наши исходные данные будут выглядеть так:


```
data.head()
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Sex</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Fare</th>
      <th>Embarked</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td> 0</td>
      <td> 3</td>
      <td> 1</td>
      <td> 28</td>
      <td> 1</td>
      <td> 0</td>
      <td>  7.2500</td>
      <td> 2</td>
    </tr>
    <tr>
      <th>1</th>
      <td> 1</td>
      <td> 1</td>
      <td> 0</td>
      <td> 28</td>
      <td> 1</td>
      <td> 0</td>
      <td> 71.2833</td>
      <td> 0</td>
    </tr>
    <tr>
      <th>2</th>
      <td> 1</td>
      <td> 3</td>
      <td> 0</td>
      <td> 28</td>
      <td> 0</td>
      <td> 0</td>
      <td>  7.9250</td>
      <td> 2</td>
    </tr>
    <tr>
      <th>3</th>
      <td> 1</td>
      <td> 1</td>
      <td> 0</td>
      <td> 28</td>
      <td> 1</td>
      <td> 0</td>
      <td> 53.1000</td>
      <td> 2</td>
    </tr>
    <tr>
      <th>4</th>
      <td> 0</td>
      <td> 3</td>
      <td> 1</td>
      <td> 28</td>
      <td> 0</td>
      <td> 0</td>
      <td>  8.0500</td>
      <td> 2</td>
    </tr>
  </tbody>
</table>
</div>



Теперь нам надо написать код для приведения проверочного файла в нужный нам вид. Для этого можно просто скопировать куски кода которые были выше(или просто написать функцию для обработки входного файла):


```
test = read_csv('Kaggle_Titanic/Data/test.csv')
test.Age[test.Age.isnull()] = test.Age.mean()
test.Fare[test.Fare.isnull()] = test.Fare.median()
MaxPassEmbarked = test.groupby('Embarked').count()['PassengerId']
test.Embarked[test.Embarked.isnull()] = MaxPassEmbarked[MaxPassEmbarked == MaxPassEmbarked.max()].index[0]
result = DataFrame(test.PassengerId)
test = test.drop(['Name','Ticket','Cabin','PassengerId'],axis=1)

label.fit(dicts['Sex'])
test.Sex = label.transform(test.Sex)

label.fit(dicts['Embarked'])
test.Embarked = label.transform(test.Embarked)
```


```
test.head()
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Pclass</th>
      <th>Sex</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Fare</th>
      <th>Embarked</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td> 3</td>
      <td> 1</td>
      <td> 34.5</td>
      <td> 0</td>
      <td> 0</td>
      <td>  7.8292</td>
      <td> 1</td>
    </tr>
    <tr>
      <th>1</th>
      <td> 3</td>
      <td> 0</td>
      <td> 47.0</td>
      <td> 1</td>
      <td> 0</td>
      <td>  7.0000</td>
      <td> 2</td>
    </tr>
    <tr>
      <th>2</th>
      <td> 2</td>
      <td> 1</td>
      <td> 62.0</td>
      <td> 0</td>
      <td> 0</td>
      <td>  9.6875</td>
      <td> 1</td>
    </tr>
    <tr>
      <th>3</th>
      <td> 3</td>
      <td> 1</td>
      <td> 27.0</td>
      <td> 0</td>
      <td> 0</td>
      <td>  8.6625</td>
      <td> 2</td>
    </tr>
    <tr>
      <th>4</th>
      <td> 3</td>
      <td> 0</td>
      <td> 22.0</td>
      <td> 1</td>
      <td> 1</td>
      <td> 12.2875</td>
      <td> 2</td>
    </tr>
  </tbody>
</table>
</div>



Ну что же терепь данные для построения и проверки модели готовы и можно приступить к ее построению. Для проверки точности модели будем использовать <a href="http://www.machinelearning.ru/wiki/index.php?title=%D0%A1%D0%BA%D0%BE%D0%BB%D1%8C%D0%B7%D1%8F%D1%89%D0%B8%D0%B9_%D0%BA%D0%BE%D0%BD%D1%82%D1%80%D0%BE%D0%BB%D1%8C">скользящий контроль</a> и <a href='http://ru.wikipedia.org/wiki/ROC-%D0%BA%D1%80%D0%B8%D0%B2%D0%B0%D1%8F'>ROC-кривые</a>. Проверку будем выполнять на обучающей выборке, после чего применим ее на тестовую.

Итак рассмотрим несколько алгоритмов машинного обучения: <ul><li>SVM (с линейным ядром и квадратичным полиномом)</li><li>Метод ближайших соседей</li><li>Random forest</li></ul>

Загрузим нужные нам библиотки:


```
from sklearn import cross_validation, svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve, auc
import pylab as pl
```

Для начала, надо разделить нашу обучаюшую выборку на показатель, который мы исследуем, и признаки его определяющие:


```
target = data.Survived
train = data.drop(['Survived'], axis=1) #из исходных данных убираем Id пассажира и флаг спасся он или нет

kfold = 5 #количество подвыборок для валидации
itog_val = {} #список для записи результатов кросс валидации разных алгоритмов
```

Теперь наша обучающая выборка выглядит так:


```
train.head()
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Pclass</th>
      <th>Sex</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Fare</th>
      <th>Embarked</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td> 3</td>
      <td> 1</td>
      <td> 28</td>
      <td> 1</td>
      <td> 0</td>
      <td>  7.2500</td>
      <td> 2</td>
    </tr>
    <tr>
      <th>1</th>
      <td> 1</td>
      <td> 0</td>
      <td> 28</td>
      <td> 1</td>
      <td> 0</td>
      <td> 71.2833</td>
      <td> 0</td>
    </tr>
    <tr>
      <th>2</th>
      <td> 3</td>
      <td> 0</td>
      <td> 28</td>
      <td> 0</td>
      <td> 0</td>
      <td>  7.9250</td>
      <td> 2</td>
    </tr>
    <tr>
      <th>3</th>
      <td> 1</td>
      <td> 0</td>
      <td> 28</td>
      <td> 1</td>
      <td> 0</td>
      <td> 53.1000</td>
      <td> 2</td>
    </tr>
    <tr>
      <th>4</th>
      <td> 3</td>
      <td> 1</td>
      <td> 28</td>
      <td> 0</td>
      <td> 0</td>
      <td>  8.0500</td>
      <td> 2</td>
    </tr>
  </tbody>
</table>
</div>



Теперь разобъем показатели полученные ранее на 2 подвыборки(обучающую и тестовую) для расчет ROC кривых (для скользящего контроля этого делать не надо, т.к. функция проверки это делает сама. В этом нам поможет функция <a href="http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.train_test_split.html#sklearn.cross_validation.train_test_split">train_test_split</a> модуля <a href="http://scikit-learn.org/stable/modules/cross_validation.html#cross-validation">cross_validation</a>:


```
ROCtrainTRN, ROCtestTRN, ROCtrainTRG, ROCtestTRG = cross_validation.train_test_split(train, target, test_size=0.25) 
```

В качестве праметров ей передается: <ul><li>Массив параметров</li><li>Массив значений показателей</li><li>Соотношение в котром будет разбита обучающая выборка (в нашем случае для тестового набора будет выделена 1/4 часть данных исходной обучающей выборки)</li></ul>.</br>На выходе функция выдает 4 массива: <ol><li>Новый обучающий массив параметров</li><li>тестовый массив параметров</li><li>Новый массив показателей</li><li>тестовый массив показателей</li></ol>

Далее представлены перечисленные методы с наилучшими параметрами подобранные опытным путем:


```
model_rfc = RandomForestClassifier(n_estimators = 80, max_features='auto', criterion='entropy',max_depth=4) #в параметре передаем кол-во деревьев
model_knc = KNeighborsClassifier(n_neighbors = 18) #в параметре передаем кол-во соседей
model_lr = LogisticRegression(penalty='l1', tol=0.01) 
model_svc = svm.SVC() #по умолчанию kernek='rbf'
```

Теперь проверим полученные модели с помощью скользящего контроля. Для этого нам необходимо вопользоваться функцией <a href="http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.cross_val_score.html#sklearn.cross_validation.cross_val_score">cross_val_score</a> 


```
scores = cross_validation.cross_val_score(model_rfc, train, target, cv = kfold)
itog_val['RandomForestClassifier'] = scores.mean()
scores = cross_validation.cross_val_score(model_knc, train, target, cv = kfold)
itog_val['KNeighborsClassifier'] = scores.mean()
scores = cross_validation.cross_val_score(model_lr, train, target, cv = kfold)
itog_val['LogisticRegression'] = scores.mean()
scores = cross_validation.cross_val_score(model_svc, train, target, cv = kfold)
itog_val['SVC'] = scores.mean()

#рисуем результат
DataFrame.from_dict(data = itog_val, orient='index').plot(kind='bar', legend=False)
```




    <matplotlib.axes.AxesSubplot at 0x5df1b10>




![png](/resource/titanic_test_post_files/Titanic_test_post_52_1.png)


Когда мы собрали данные по перекрестным проверкам, давайте нарисуем графики ROC-кривых:


```
pl.clf()
plt.figure(figsize=(8,6))
#SVC
model_svc.probability = True
probas = model_svc.fit(ROCtrainTRN, ROCtrainTRG).predict_proba(ROCtestTRN)
fpr, tpr, thresholds = roc_curve(ROCtestTRG, probas[:, 1])
roc_auc  = auc(fpr, tpr)
pl.plot(fpr, tpr, label='%s ROC (area = %0.2f)' % ('SVC', roc_auc))
#RandomForestClassifier
probas = model_rfc.fit(ROCtrainTRN, ROCtrainTRG).predict_proba(ROCtestTRN)
fpr, tpr, thresholds = roc_curve(ROCtestTRG, probas[:, 1])
roc_auc  = auc(fpr, tpr)
pl.plot(fpr, tpr, label='%s ROC (area = %0.2f)' % ('RandonForest',roc_auc))
#KNeighborsClassifier
probas = model_knc.fit(ROCtrainTRN, ROCtrainTRG).predict_proba(ROCtestTRN)
fpr, tpr, thresholds = roc_curve(ROCtestTRG, probas[:, 1])
roc_auc  = auc(fpr, tpr)
pl.plot(fpr, tpr, label='%s ROC (area = %0.2f)' % ('KNeighborsClassifier',roc_auc))
#LogisticRegression
probas = model_lr.fit(ROCtrainTRN, ROCtrainTRG).predict_proba(ROCtestTRN)
fpr, tpr, thresholds = roc_curve(ROCtestTRG, probas[:, 1])
roc_auc  = auc(fpr, tpr)
pl.plot(fpr, tpr, label='%s ROC (area = %0.2f)' % ('LogisticRegression',roc_auc))
pl.plot([0, 1], [0, 1], 'k--')
pl.xlim([0.0, 1.0])
pl.ylim([0.0, 1.0])
pl.xlabel('False Positive Rate')
pl.ylabel('True Positive Rate')
pl.legend(loc=0, fontsize='small')
pl.show()
```


    <matplotlib.figure.Figure at 0x6280dd0>



![png](/resource/titanic_test_post_files/Titanic_test_post_54_1.png)


Как видно по результам обоих проверок алгоритм на основе деревьев решений выдал лучшие результаты, чем остальные. Теперь осталось только применить нашу модель к тестовой выборке:


```
model_rfc.fit(train, target)
result.insert(1,'Survived', model_rfc.predict(test))
result.to_csv('Kaggle_Titanic/Result/test.csv', index=False)
```
