+++
date = "2016-02-29T13:26:13+03:00"
draft = false
title = "Написание контроллера для OpenERP"

+++

### Задача
После довольно продолжительной работы с OpenERP, я понял, что это довольно гибкая система для очень широкого круга задач. Но у нее есть один, на мой взгляд, недостаток - в ней нет встроенное хорошей аналитики. Да есть возможность стоить графики и отчеты, но это не совсем такая аналитика, как мне понадобилась.

Поэтому, после некоторых размышлений, я решил связать OpenERP c <a href="http://cubes.databrewery.org">Cubes Framework</a> (это <a href="https://habrahabr.ru/post/222421/">фрейворк для создания OLAP кубов на Python</a>). Тем более что для него существует готовый простомотрщик <a href="http://jjmontesl.github.io/cubesviewer/">CubesViewer</a>, который реализован на html+js. 
Для свзяки <b>CubesViewer</b> и <b>OpenERP</b> я придумал создать контроллер, который приобращении к OpenERP по адресу <code>/olap_analytics</code> выдавал бы страницу с работающим CubesViewer'ом.

### Подготовка
Для начала скачаем репозиторий с CubesViewer'ом:
```
git clone https://github.com/jjmontesl/cubesviewer.git
```

Теперь создадим заготовку пустого модуля OpenERP. Для этого в папке создами новую папку следующей структуры:

* static/
  * css
  * js
  * img
* controllers/
  * ```__init__.py```
  * ```olap_controller.py``` (тут будет находится код нашего контроллера)
* ```__init__.py```
* ```__openerp__.py```

Файл ```__openerp__.py``` следующего содержания:

``` python
{
    'name': 'Cubes integration',
    'category': 'Hidden',
    'version': '7.0.1.0',
    'description': "",
    'depends': ['web'],
    'data': [],
    'installable': True,
    'auto_install': False,
}
```

Файл ```/controllers/__openerp__.py/``` следующего содержания:

``` python
# -*- coding: utf-8 -*-

import olap_controller
```

Файл <i><span class="underline"><span class="underline">init</span></span>.py</i> следующего содержания:
``` python
# -*- coding: utf-8 -*-

import controllers
```

После создания заготовки модуля необходимо статические файлы cubesviewer'а перенести в соответствующие поддиректории папки <b>static/</b> нашего модуля.
Затем нужно скопировать из репозитория <b>cubesviewer/src/htmlviews/gui.html</b> в папку <b>controllers/</b> созданного модуля OpenERP.
И в заключении в скопированном файле <b>controllers/gui.html</b> поменять начало путей статических файлов с <b>href="../web/static&#x2026;</b> на <b>href="<code>/&lt;имя_модуля&gt;/static...</code></b>


### Создание контроллера
Итак, когда приготовления готовы, можно приступить к созданию контроллера. Для этого в папке <i>controllers</i> у нашего модуля разместим создадим файл ```olap_controller.py``` со следующим содержимым:

``` python
# -*- coding: utf-8 -*-

import openerp.addons.web.http as http


class OlapController(http.Controller):
    _cp_path = '/olap_analytics'

    @http.httprequest
    def index(self, req, s_action=None, **kw):
        html_tempale = open('gui.html')
        render_html = html_tempale.read()
        return render_html

```

Здесь мы создаем класс контроллера OpenERP, и в переменной ```_cp_path``` указываем абсолютный адрес, который будет обрабатывать наш контроллер. 

Декоратор <b>@http.httprequest</b> отвечет за рендеринг страницы ответа на стороне сервера (в нашем случает это html-страница, но также это может быть и json-объект).

В  функции <b>index</b> бы считываем шаболон страницы с отображение аналитики и отдаем результат чтения на рендеринг для ответа.



### Заключение
Выше был описан пример создания рабочего контролера для OpenERP. Готовый модуль итеграции с Cubes - <a href="https://github.com/kuznetsovin/cubes_integration">cubes_integration</a> - нахаодится в моем GitHub'е. Возможно есть и другие способы подружить эти две системы, но реализовать их не удалось, да и в Интернете по этой теме практически нет ничего полезного. Так что думаю для кого-нибудь данное творение будет полезным.

