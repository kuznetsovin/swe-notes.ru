+++
date = "2016-02-05T13:26:13+03:00"
draft = false
title = "Настройка ГОСТ для OpenSSL и работа с ним"

+++

Недавно на работе возникла необходимость настроить генерацию ключевой пары и
запроса на сертификат x.509 с помощью OpenSSL и алогритмов ГОСТ. После
изучения материала по данной теме было найдено определенное решение и
построен соответсвующий алгоритм действий, который и хотелось бы описать.


### Настройка OpenSSL
Итак для генерации ключей по ГОСТ у OpenSSL идет библиотека
<i>libgost.so</i>, которая поставляется с OpenSSL, не не входит в
конфигурацию по умолчанию. Подробнее можно прочитать <a href="https://github.com/luvit/openssl/blob/master/openssl/engines/ccgost/README.gost">здесь</a>. В кратце
порядок дествий следующий:

* в глобальную секцию настроек добавить

	```
	openssl_conf = openssl_def
	```

* Затем добавить секцию с описание заданной конфигурации

	```
	[openssl_def]                                                                   
	engines = engine_section
	```

* Теперь надо дабавить секцию с описание движка шифрования

	```
	[engine_section]                                                                
	gost = gost_section
	```

* И напоследок добавть секцию с описание параметров указанного выше движка

	```
	[gost_section]                                                                  
	engine_id = gost                                                                
	dynamic_path = /usr/lib/ssl/engines/libgost.so                                  
	default_algorithms = ALL                                                        
	CRYPT_PARAMS = id-Gost28147-89-CryptoPro-A-ParamSet 
	```

### Расширение OpenSSL сертификата дополнительными полями:
В файле <i>openssl.conf</i> нужно:

* Задать секцию расширений

	```
	req_extensions = v3_req # The extensions to add to a certificate request
	```

* Описать поля, которые будут входить в секцию  расширений

	```
	[ v3_req ]
	certificatePolicies = 1.2.643.100.113.1, 1.2.643.100.113.2
	1.2.643.100.111 = ASN1:UTF8String:ATLAS NCM
	keyUsage = critical, Digital Signature, Non Repudiation, Key Encipherment, Data Encipherment
	subjectAltName = DER:3014A012060355042DA00B0309000400FFFFFFFFFFFF
	```

* Для добавления кастомных полей в поле <i>subject</i> сертификата, в секцию <i>[ new<sub>oids</sub> ]</i> вписать нужные поля, например:

	```
	[ v3_req ]
	inn = 1.2.643.3.131.1.1
	ogrn = 1.2.643.100.1
	```


### Основные команды по работе с OpenSSL:

Сгенерировать закрытый ключ c помощью ГОСТ:

```
openssl genpkey -algorithm gost2001 -pkeyopt paramset:A -out file.key
```

Вытащить закрытый ключ из полученного файла (без первых 3-х байт):

```
openssl asn1parse -in file.key -offset 35 -out key
```

Генерируем запрос на сертификат:

```
openssl req -new -key file.key -subj "сабжект" -out file.req
```

Для самоподписанного сертификатв выполнить для запроса команду:

```
openssl x509 -req -days 365 -in file.req -signkey test.key -out cert.cer
```

Посмотреть сертификат можно:

```
openssl x509 -in cert.cer -noout -text
```

Выгрузить в контейнер PKCS#12:

```
openssl pkcs12 -export -out test.p12 -in cert.cer -inkey test.key
```
