+++
date = "2017-02-06T09:35:07+03:00"
title = "Настройка асинхронной потоковой рекпликации PostgreSQL"
+++

В предыдущей статье по [масштабированию Odoo]({{< relref "post/odoo_ha.md" >}}), не был рассмотрен вопрос рекпликации данных БД. Поэтому для восполения этого я описанл процемм настройки простой репликации ниже.

### Типы репликаций 
Рекпликация в PostgreSQL бывает двух видов:

*  **hot standby** - репликация может и читать и писать
*  **warm standby** - репликация может только читать

Так же репликации могут быть асинхронными и синхронными. Более подробно можно прочитать [1]. 

Про рекпликацию данных можно почитать [здесь](https://ruhighload.com/post/Репликация+данных). 

Далее речь пойдет про *hot standby*.

### Подготовка master
В файл *pg_hba.conf* добавляем строку:
```
host    replication      odoo       10.0.2.0/24            md5
```
Эта строчка нужна для работы ```pg_basebackup```. Настройка реплики будет проводится для пользователя *odoo*.

Правим /etc/postgresql/9.4/main/postgresql.conf:
```
# какие адреса слушать, замените на IP сервера
listen_addresses = '*'

wal_level = hot_standby

# это нужно, чтобы работал pg_rewind
wal_log_hints = on

max_wal_senders = 8
wal_keep_segments = 64

hot_standby = on
```

После этого перезапускаем postgres.
```
systemctl restart postgresql
```

### Настройка slave
Для настройки реплики у вас на slave должен быть пользователь odoo.

Останавливаем демон PostgreSQL
```
systemctl stop postgresql
```

Под пользователем *postgres* переливаем данные с мастера:
```
su - postgres
cd /var/lib/postgresql/9.4/

tar -cvzf main_backup-`date +%s`.tgz main
rm -rf main
mkdir main
chmod go-rwx main

pg_basebackup -P -R -X stream -c fast -h 10.0.2.7 -U odoo -D ./main
```

В /var/lib/postgresql/9.4/main/recovery.conf дописываем:
```
recovery_target_timeline = 'latest'
```

Когда у нас упадет мастер и мы запромоутим реплику до мастера, этот параметр позволит тянуть данные с него. Подробнее можно почитаь в [официальной документации](http://www.postgresql.org/docs/current/static/recovery-target-settings.html)

Файлы *pg_hba.conf* и *postgresql.conf* на slave должны быть аналогичны настройкам на master, за исключением ```listen_addresses```, если он не утановлен в ```*```

Запускаем сервер Postgres:
```
systemctl start postgres
```

### Проверка работы реплики

На мастере выполняем запрос:
```
SELECT * FROM pg_stat_replication;
```

Должны увидеть, что реплика действительно забирает WAL:

```
-[ RECORD 1 ]----+------------------------------
pid              | 1306
usesysid         | 16385
usename          | odoo
application_name | walreceiver
client_addr      | 10.0.2.8
client_hostname  |
client_port      | 52822
backend_start    | 2017-02-06 10:47:50.309435+03
backend_xmin     |
state            | streaming
sent_location    | 0/6000138
write_location   | 0/6000138
flush_location   | 0/6000138
replay_location  | 0/6000138
sync_priority    | 0
sync_state       | async
```

На реплике:
```
sudo less /var/log/postgresql/postgresql-9.4-main.log
```

Должны увидеть что-то на тему «read only connections»:
```
LOG:  entering standby mode
LOG:  redo starts at 0/2000028
LOG:  consistent recovery state reached at 0/20000F8
LOG:  database system is ready to accept read only connections
LOG:  started streaming WAL from primary at 0/3000000 on timeline 1
```

Переключить slave в режим master можно следующей командой:
```
/usr/lib/postgresql/9.4/bin/pg_ctl promote -D /var/lib/postgresql/9.4/main/
```

Используемы материалы:

1. [Потоковая репликация в PostgreSQL и пример фейловера](http://eax.me/postgresql-replication/)
2. High availability OpenERP Documentation
3. [Streaming Replication](https://wiki.postgresql.org/wiki/Streaming_Replication)
4. [High Availability, Load Balancing, and Replication](https://www.postgresql.org/docs/current/static/high-availability.html)
