+++
date = "2018-03-18T16:06:36+03:00"
title = "Настройка DroneCI + Odoo"
+++

В [предыдущей](/post/self-server-delpoy/) статье я описал как настроить собственный сервер разработки. Недавно мне потребовалось привязать к нему проект на Odoo и я столкунулся с некоторыми трудностями, которые нужно зафиксировать.

Файл для конфигурации `.drone.yml` для конфигурации Drone будет следующий:
```
workspace:
  base: /mnt
  path: extra-addons

pipeline:
  build:
    image: kuznetsovin/drone-odoo
    commands:
      - sleep 60
      - odoo -d sport_event --db_host postgres -r odoo -w odoo -i sport_event --stop-after-init

services:
  postgres:
    image: postgres:10
    environment:
      - POSTGRES_USER=odoo
      - POSTGRES_PASSWORD=odoo

clone:
  git:
    image: plugins/git
    depth: 50
    environment:
      - DRONE_NETRC_MACHINE=<git_server_address>
      - DRONE_NETRC_USERNAME=<user_key_from_droneci_db>
      - DRONE_NETRC_PASSWORD=x-oauth-basic
```

Как видно я беру не стандартный образ Odoo из DockerHub, а сделал свой кастомизированный, т.к. DroneCI, на данный момент необходимо чтобы контейнер работал от *root*.

Также добавлен раздел `clone` для работы с приватными репозиториями Gogs.
