+++
date = "2014-05-14T17:20:02+03:00"
draft = false
title = "Организация OLAP куба средствами Python"

+++

Добрый день, уважаемые читатели.
Сегодня я расскажу вам о том, как можно построить простенькую систему анализа данных на Python. В этом мне помогут framework <a href="http://cubes.databrewery.org/">cubes</a> и  пакет <a href="http://jjmontesl.github.io/cubesviewer/">cubesviewer</a>.
 
<strong>Сubes</strong> представляет собой framework'ом для работы с многомерными данными с помощью Python. Кроме того он включает в себя OLAP HTTP-сервер для упрощенной разработки приложений отчетности и общего просмотра данных. 
 
<strong>Сubesviewer</strong> представляет собой web-интерфейс для работы с вышеуказанным сервером. 

### Установка и настройка cubes
Для начала надо установить библиотеки, необходимые для работы пакета:

```
pip install pytz python-dateutil jsonschema
pip install sqlalchemy flask
```
Далее устанавливаем сам пакет <strong>cubes</strong>:

```
pip install cubes
```

Как показала практика, лучше использовать версию (1.0alpha2) из текущего <a href="https://github.com/Stiivi/cubes">репозитория</a>.

##### Доп настройки под windows

Если вы планируете работать под Windows необходимо в файле <em>PYTHON_DIR\Lib\site-packages\dateutil\tz.py</em> заменить 40 строку:

```
return myfunc(*args, **kwargs).encode()
```

на 

```
return myfunc(*args, **kwargs)
```

Затем, вне зависимости от платформы на которой вы работаете, нужно добавить следующий <a href="https://github.com/rgruebel/cubes/commit/4fb6b8e1d85a99bc7bdd4f88697ca6731503eee6">fix</a> для корректной работы json-парсера. Вносить его надо в <em>PYTHON_DIR\Lib\site-packages\cubes-1.0alpha-py2.7.egg\cubes\metadata.py</em> начиная с 90 строки:

```
     # TODO: same hack as in _json_from_url
     return read_model_metadata_bundle(source)
```

 ### Описание настройки куба и процесс его разворачивания

Для примера возьмем OLAP-куб, который идет в поставке с <strong>cubes</strong>. Он находится в папке <em>examples/hello_world</em> (ее можно взять с репозитория).
Наибольший интерес для нас представляют 2 файла:
 
* <strong>slicer.ini</strong> - файл настроек http сервера нашего куба
* <strong>model.json</strong> - файл с описание модели куба

Остановимся на них поподробнее. Начнем с файла <strong>slicer.ini</strong>, который может включать следующие разделы:
 
* ```[workspace]``` – конфигурация рабочего места
* ```[server]``` - параметры сервера (адрес, порт и тд.)
* ```[models]``` - список моделей для загрузки
* ```[datastore] или [store]``` – параметры хранилища данных
* ```[translations]``` - настройки локализации для модели.

Итак разберем из нашего тестового файла видно, что сервер будет располагаться на локальной машине и будет работать по 5000 порту. В качестве хранилища будет использоваться локальная база SQLite под названием data.sqlite. 

Подробнее о конфигурировании сервера можно <a href="http://pythonhosted.org/cubes/configuration.html">прочитать</a> в документации.
Также из файла видно, что описание модели нашего куба находиться в файле <strong>model.json</strong> , описание структуры которого мы сейчас и займемся.
Файл описания модели, это <em>json</em>-файл, который включает следующие логические разделы:

* ```name``` – имя модели
* ```label``` – метка
* ```description``` – описание модели
* ```locale``` – локаль для модели (если задана локализация)
* ```cubes``` – список метаданных кубов
* ```dimensions``` – список метаданных измерений
* ```public_dimensions``` – список доступных измерений. По умолчанию все измерения доступны.

Для нас представляют интерес разделы <em>cubes</em> и <em>dimensions</em>, т.к. все остальные опциональны.
Элемент списка <em>dimensions</em>, содержит следующие метаданные:

<table border="1">
<tr><th>Ключ</th><th>Описание</th></tr>
<tr><td>name</td><td>идентификатор измерения</td></tr>
<tr><td>label</td><td>Имя измерения видное пользователю</td></tr>
<tr><td>description</td><td>описание измерения для пользователей</td></tr>
<tr><td>levels</td><td>Список уровней измерений</td></tr>
<tr><td>hierarchies</td><td>Список иерархий</td></tr>
<tr><td>default_hierarchy_name</td><td>Идентификатор иерархии</td></tr>
</table>

Элемент списка <em>cubes</em>, содержит следующие метаданные:

<table border="1">
<tr><th>Ключ</th><th>Описание</th></tr>
<tr><td>name</td><td>идентификатор измерения</td></tr>
<tr><td>label</td><td>Имя измерения видное пользователю</td></tr>
<tr><td>description</td><td>описание измерения для пользователей</td></tr>
<tr><td>dimensions</td><td>список имен измерений заданных выше</td></tr>
<tr><td>measures</td><td>список мер</td></tr>
<tr><td>aggregates</td><td>список функций агрегации мер</td></tr>
<tr><td>mappings</td><td>задание разметки логических и физических атрибутов</td></tr>
</table>

Исходя из выше описанного, можно понять, что у нас в модели в будет 2 измерения (<strong><em>item, year</em></strong>). У измерения "<em>item</em>" 3 уровня измерений:
 
* <strong><em>category</em></strong>. Отображаемое имя "Category", поля <em>"category", "category_label"</em>
* <strong><em>subcategory</em></strong>. Отображаемое имя "Sub-category", поля <em>"subcategory", "subcategory_label"</em>
* <strong><em>line_item</em></strong>. Отображаемое имя "Line Item", поле <em>"line_item"</em>
 
В качестве меры в нашем кубе будет выступать поле <em>"amount"</em>, для которой выполняются функции суммы и подсчета кол-ва строк.
Подробнее о разметке модели куба можно почитать в <a href="http://pythonhosted.org/cubes/model.html#introduction">документации</a>
После того, как мы разобрались с настройками, надо создать нашу тестовую базу. Для того, чтобы это сделать, необходимо запустить скрипт <em>prepare_data.py</em>:

```
python prepare_data.py
```

Теперь осталось только запустить наш тестовый сервер с кубом, который называется <em>slicer</em>:

```
slicer serve slicer.ini
```

После этого можно проверить работоспособность нашего куба. Для этого в строке браузера можно ввести:
<em>http://localhost:5000/cube/irbd_balance/aggregate?drilldown=year</em> 
В ответ мы получим json-объект с результатом агрегации наших данных. Подробнее о формате ответа сервера можно почитать <a href="http://pythonhosted.org/cubes/server.html">тут</a>.

### Установка cubesviewer 
Когда мы настроили наш куб, можно приступить к установке <strong>сubesviewer</strong>. Для этого надо скопировать <a href="https://github.com/nonsleepr/cubesviewer">репозиторий</a> себе на диск:

```
git clone https://github.com/nonsleepr/cubesviewer.git
```

А потом просто переместить содержимое папки <em>/src</em> в нужный место. 

Надо отметить, что <em>сubesviewer</em> является Django-приложением, поэтому <strong>для его работы необходим Django (не выше версии 1.4)</strong>, а также пакеты <strong>requests</strong> и <strong>django-piston</strong>. Т.к. данная версия Django уже устарела, то выше я привел ссылку откуда можно взять сubesviewer для версии Django 1.6.

Установка ее немного отличается от оригинала тем, что в файл конфигурации сервера <code>slicer.ini</code> в раздел <code>[server]</code> нужно добавить строку <code>allow_cors_origin: http://localhost:8000</code>

После этого надо <a href="https://github.com/jjmontesl/cubesviewer/blob/master/doc/guide/cubesviewer-gui-installation.md">настроить</a> приложение в файле <em>CUBESVIEWER_DIR/web/cvapp/settings.py</em>. Указав ему настройки БД, адрес OLAP сервера (переменная <code>CUBESVIEWER_CUBES_URL</code>) и адрес просмоторщика (<code>CUBESVIEWER_BACKEND_URL</code>)

Осталось внести небольшой <a href="https://bitbucket.org/spookylukey/django-piston/commits/40645e760ea2cb9a37d87c9352607b3fa7fac346#chg-piston/emitters.py">fix</a> в <em>dajno-piston</em>

Теперь можно синхронизировать наше приложение с БД. Для этого из <em>CUBESVIEWER_DIR/web/cvapp</em> нужно выполнить:

```
python manage.py syncdb
```

Осталось запустить локальный сервер Django

```
python manage.py runserver
```

Теперь осталось зайти на указанный в <code>CUBESVIEWER_BACKEND_URL</code> адрес через браузер. И наслаждаться готовым результатом.

### Заключение
Для иллюстрации работы я взял самый простой пример. Надо отметить что для производственных проектов <em>cubes</em> можно <a href="http://pythonhosted.org/cubes/deployment.html">развернуть</a> например на <a href="http://httpd.apache.org/">apache</a> или <a href="http://uwsgi-docs.readthedocs.org/en/latest/">uswgi</a>. Ну а подключить к нему <em>сubesviewer</em> с помощью этой статьи не составит труда.

Если тема будет интересна сообществу, то я раскрою ее в одной из будущих статей.
