+++
date = "2018-02-11T16:06:36+03:00"
title = "Настройка собственного сервера для разработки"

+++

Недавно я принял решение перенести все свои разработки в персональный репозиторий, так как несколько моих проектов находятся в приватных репозиториях на bitbucket, а он после Нового года некоторое время работал нестабильно. 

Сначала я рассматривал GitLab, но он оказался слишком требователен к ресурсам, а за них переплачивать хостеру мне не хотелось. И поэтому я принял решение собирать все по частям.

## Компоненты

В качестве системы контроля версий, в данный момент, использую git, поэтому после некоторых поисков я нашел web сервис [Gogs][1].

*Gogs* довольно приятно сделанная система внешне очень похожая на Github, причем есть возможность также заводить пользователей, организации, делать публичные и приватные репозитории.

Также я давно хотел прикрутить к своим проектам систему непрерывной интеграции и выбор пал на [Drone][2]. Система работает с doсker контейнерами, что очень удобно при сборках и деплой.

Обе описанные выше системы написаны на Go и не требовательны к ресурсам.

Для отслеживания ошибок в работающих проектах я развернул [Sentry][3].

## Разворачивание

Для упрощения деплоя и администрирования всего это "добра" все компоненты я поместил в docker контейнеры и написал compose файл:

```
version: '3'

services:    
  postgresql:
    image: postgres:latest
    container_name: postgresql
    restart: always
    environment:
       - POSTGRES_USER=${POSTGRES_USER}
       - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
       - POSTGRES_MULTIPLE_DATABASES=gogs,drone,sentry
    ports:
      - 5432:5432

    volumes:
      - ./docker-postgresql-multiple-databases:/docker-entrypoint-initdb.d
      - /var/lib/postgresql:/var/lib/postgresql
      - /var/lib/postgresql/data:/var/lib/postgresql/data
       
  gogs:
    image: gogs/gogs:latest
    container_name: gogs
    restart: always
    ports:
      - 3000:3000
      - "10022:22"
    volumes:
      - /var/lib/gogs:/data
    depends_on:
       - postgresql

  drone-server:
    image: drone/drone:latest
    container_name: drone_server
    restart: always
    ports:
      - 8000:8000
      - 9000:9000
    volumes:
      - /var/lib/drone:/var/lib/drone/
    environment:
      - DRONE_OPEN=true
      - DRONE_HOST=http://${DRONE_HOST}:8000
      - DRONE_GOGS=true
      - DRONE_GOGS_PRIVATE_MODE=true
      - DRONE_GOGS_URL=https://${GOGS_SERVER}
      - DRONE_SECRET=${DRONE_SECRET}
      - DRONE_DATABASE_DRIVER=postgres
      - DRONE_DATABASE_DATASOURCE=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@postgresql:5432/drone?sslmode=disable
    depends_on:
      - gogs
      - postgresql
      
  drone-agent:
    image: drone/agent:latest
    container_name: drone_agent
    command: agent
    restart: always
    depends_on:
      - drone-server
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      - DRONE_SERVER=drone-server:9000
      - DRONE_SECRET=${DRONE_SECRET}

  registry:
    image: registry:2
    container_name: docker_registry
    restart: always
    ports:
      - 5000:5000
    volumes:
      - /var/registry:/var/lib/registry

  redis:
    image: redis
    container_name: redis
    restart: always

  sentry:
    image: sentry
    container_name: sentry
    restart: always
    ports:
      - 9090:9000
    depends_on:
      - redis
      - postgresql
    environment:
      SENTRY_SECRET_KEY: ${SENTRY_SECRET}
      SENTRY_POSTGRES_HOST: postgresql
      SENTRY_DB_NAME: sentry
      SENTRY_DB_USER: ${POSTGRES_USER}
      SENTRY_DB_PASSWORD: ${POSTGRES_PASSWORD}
      SENTRY_REDIS_HOST: redis

  cron:
    image: sentry
    container_name: sentry_cron
    links:
     - redis
     - postgresql
    command: "sentry run cron"
    environment:
      SENTRY_SECRET_KEY: ${SENTRY_SECRET}
      SENTRY_POSTGRES_HOST: postgresql
      SENTRY_DB_USER: sentry
      SENTRY_DB_PASSWORD: sentry
      SENTRY_REDIS_HOST: redis

  worker:
    image: sentry
    container_name: sentry_worker
    links:
     - redis
     - postgresql
    command: "sentry run worker"
    environment:
      SENTRY_SECRET_KEY: ${SENTRY_SECRET}
      SENTRY_POSTGRES_HOST: postgresql
      SENTRY_DB_USER: sentry
      SENTRY_DB_PASSWORD: sentry
      SENTRY_REDIS_HOST: redis

```

Надо уточнить что для корретного создания нескольких баз контейнером postgresql, необходимо использовать [docker-postgresql-multiple-databases](https://github.com/mrts/docker-postgresql-multiple-databases).

После запуска данного compose файла на моем сервере развернулось все, для ведения разработки на своем сервере, который зависит только от провайдера vds.

## Примечание

Для корректной работы *drone ci* в связке с приватным репозиториями *gogs*, в файле .drone.yml необходимо указать следующий раздел:

```
clone:
  git:
    image: plugins/git
    depth: 50
    environment:
      - DRONE_NETRC_MACHINE={сервер gogs}
      - DRONE_NETRC_USERNAME={ключ пользователя в drone из БД}
      - DRONE_NETRC_PASSWORD=x-oauth-basic

```

## Заключение

После ряда не хитрых действий получился автономный сервер разработки, не зависящий от вендоров и сделанный на основе open-source решений. При этом, в отличие от Gitlab данная конфигурация не особо требовательна к ресурсам и может удачно работать на машине с 2Гб RAM.

## Полезные ссылки:
1. [Gogs][1]
2. [Drone][2]
3. [Sentry][3]

[1]: https://gogs.io
[2]: https://drone.io
[3]: https://sentry.io/welcome/
