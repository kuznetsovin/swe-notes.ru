+++
date = "2016-11-24T16:52:02+03:00"
title = "Уствновка и настройка OSRM"

+++

OSRM (Open Source Routing Machine) - проект, с открытым исходным кодом, который позволяет развернуть у себя на сервере, свой собственный сервис построения маршрутов.

### Установка

Для начала устанавливаем нужные пакеты:
```
sudo apt-get install git libboost-dev gcc g++ make cmake libstxxl-dev libxml2-dev libbz2-dev zlib1g-dev libzip-dev libboost-filesystem-dev libboost-thread-dev libboost-system-dev libboost-regex-dev libboost-program-options-dev libboost-iostreams-dev libgomp1 libpng-dev libprotobuf-dev protobuf-compiler liblua5.1-0-dev libluabind-dev pkg-config libosmpbf-dev libboost-test1.55-dev libtbb-dev libexpat1-dev libxxf86vm1 libxxf86vm-dev libxi-dev libxrandr-dev
```

Далее клонируем репозиторий *Project-OSRM*:
```
git clone https://github.com/Project-OSRM/osrm-backend.git
cd osrm-backend
```

и выполняем команды, необходимые для сборки:
```
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
cmake --build .
sudo cmake --build . --target install
```
Чтобы запустить сервер, ему необходимо подготовить данные, для построения графа. Чтобы загрузить их, необходимо их извлечь из карт OpenStreetMap. Делается это, входящими в комплект утилитами.

### Настройка и запуск

Для начала скачаем файл osm с нужным регионом (сделать это можно с сайта OpenStreetMap) и с помощью утилиты ```osrm-extract```, извлекаем и сохраняем из него данные для дальнейшей загрузки.
```
osrm-extract -p osrm-backend/profile.lua map.osm
```
Если у вас при этой операции возникла ошибка сегментации, рекомендую обратиться у официальному мануалу (ссылка в конце статьи)

Далее необходимо извлечь иерархию и граф маршрутов из полученного osrm файла. Это делается с помощью утилиты ```osrm-contract```: 

```
osrm-contract map.osrm
```

Для запуска сервера нужно выполнить команду:
```
osrm-routed map.osrm
```

### Заключение

Данная статья это конспект [официального мануала](https://github.com/Project-OSRM/osrm-backend/wiki/Running-OSRM).
