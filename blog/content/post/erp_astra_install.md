+++
date = "2016-02-04T13:26:13+03:00"
draft = false
title = "Установка OperERP 7 на Astra Linux"
+++


### Описание настраиваемы систем
<b>OpenERP</b> (теперь <a href="https://www.odoo.com">Odoo</a>) это свободно-распространяемая ERP и CRM система с большим количеством готовых модулей. 
При необходимости модули к ней можно дописывать самому. Система написана на Python 2.7 и, соответственно на нем же
и создаются модули для нее.

<a href="http://www.astralinux.com">Astra Linux</a> - это отечественный дистрибутив на основе Debian, сертифицированный по ФСБ и ФСТЭК.

### Пошаговая установка
Итак, шаги установки следующие:

* Установка astra linux
* Добавляем репозиторий яндекса в apt:
```
deb http://mirror.yandex.ru/debian/ stable main contrib non-free
```
* Добавить pgp ключ для репозитория (необязатльно):
```
apt-get install debian-archive-keyring
```
* Устанавливаем нужные пакеты:
```
apt-get install debian-archive-keyring
apt-get install python-dateutil python-feedparser python-gdata python-ldap python-libxslt1 python-lxml python-mako \
python-openid python-psycopg2 python-pybabel python-pychart python-pydot python-pyparsing python-reportlab \
python-simplejson python-tz python-vatnumber python-vobject python-webdav python-werkzeug python-xlwt python-yaml \
python-zsi python-docutils python-psutil bzr wget python-unittest2 python-mock python-jinja2 python-pip python-dev libxslt-dev mercurial
```
* Обновляем пакет gdata и lxml:
```
sudo pip install gdata lxml --upgrade
```
* Создать пользователя postgres:
```
sudo su - postgres
createuser --createdb --username postgres --no-createrole --no-superuser --pwprompt openerp
```
* В файле pg_hba.conf заменить
```
# “local” is for Unix domain socket connections only
local all all peer
```
на строку:
```
#”local” is for Unix domain socket connections only
local all all md5
```
* Перезапустить postgres:
```
/etc/init.d/postgresql restart
```
* Cоздать пустую базу:
```
createdb -U openerp Rights
```
* Установить пакеты python:
```
dpkg -i stomp.py_3.1.1_all.deb
pip install requests stompest
```
* Изменить лимиты на размер файлов в <b>/etc/security/limits.conf</b> закоментировать строчки:
```
* hard fsize 50000000
* soft fsize 25000000
```
* Устанавливаем OpenERP (Odoo):
```
apt-get install odoo
```
* Настраиваем <a href="https://www.odoo.com/documentation/8.0/reference/cmdline.html#reference-cmdline-config">конфигурационный файл</a> по адресу */etc/odoo/openerp-server.conf*
* Запускаем odoo:
```
/etc/init.d/odoo restart
```
