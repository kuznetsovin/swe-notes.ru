+++
title = "Кастомизация заголовка в Odoo"
date = "2019-03-20T18:06:29+03:00"
+++

Мини заметка, как можно кастомизировать заголовк страницы в Odoo, а также как скрыть футер в боковом меню. 

## Скрыть футер бокового меню

Нужно изменить отнаследовать шаблон *web.menu_secondary*, и удалить класс *o_sub_menu_footer*:

```xml
<template id="menu_secondary_hide_footer" inherit_id="web.menu_secondary" name="Submenu hide footer">
        <xpath expr="//div[@class='o_sub_menu_footer']" position="replace"/>
</template>
```

## Заменить слово Odoo в заголовке страницы

Для этого надо отнаследовать компонент *WebClient* и изменить его следующим образом:

```js
odoo.define('theme_customize.WebClient', function (require) {
    "use strict";

    var WebClient = require('web.WebClient');

    return WebClient.include({
        init: function(parent) {
            this._super(parent);
            this.set('title_part', {"zopenerp": "ПСД"});
        },
    });
});
```

затем добавить это файл в загрузку страницы, через изменение представления *web.assets_backend*

## Заменить favicon

Необходимо в шаблоне *web.layout* изменить ссылку на favicon, сделать это можно через наследование:

```xml
<template id="psd_layout" inherit_id="web.layout" name="Psd layout">
    <xpath expr="//head//link" position="replace">
        <link type="image/x-icon" rel="shortcut icon" href="/module_name/path/to/file"/>
    </xpath>
</template>
```