+++
date = "2017-05-21T21:17:23+03:00"
title = "Контакты"

+++

**Email**: [kuznetsovin@gmail.com](mailto:kuznetsovin@gmail.com)

**Telegram**: [@kuznetsovin](https://t.me/kuznetsovin)

**Skype**: igor_n_kuznetsov

**Bitbucket**: [kuznetsovin](https://bitbucket.org/kuznetsovin)

**Github**: [kuzntesovin](https://github.com/kuznetsovin)

**VK**: [kuznetsovin](https://vk.com/kuznetsovin)

**Habr**: [kuznetsovin](https://habrahabr.ru/users/kuznetsovin/)

**Linkedin**: [kuznetsovin](https://linkedin.com/in/kuznetsovin)

**Twitter**: [kuznets0v1n](https://twitter.com/kuznets0v1n)

**Facebook**: [kuznets0v1n](https://facebook.com/kuznets0v1n)

**Upwork**: [Igor Kuznetsov](https://www.upwork.com/o/profiles/users/_~013eefdef5e03b0ce3/)
